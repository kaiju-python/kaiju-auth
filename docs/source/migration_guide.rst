Migration guide
---------------

Here are the changes from **kaiju-db-v1**.

`aiohttp-sessions` and HTTP session backend is no longer used. Context based sessions added (see `Session objects`_).

Session argument is no longer required in RPC method parameters and should be removed.

.. code-block:: python

  async def old_way(self, session, data):  # no longer supported
    ...

  async def new_way(self, data):
    session = self.app.get_session()

`ServerSessionFlag` is no longer available. All internal calls now will have `None` instead of a session.
