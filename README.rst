
.. image:: https://badge.fury.io/py/kaiju-auth.svg
    :target: https://pypi.org/project/kaiju-auth
    :alt: Latest package version

.. image:: https://readthedocs.org/projects/kaiju-auth/badge/?version=latest
    :target: https://kaiju-auth.readthedocs.io/en/latest/?badge=latest
    :alt: Documentation Status

.. image:: https://img.shields.io/badge/code%20style-black-000000.svg
   :target: https://github.com/psf/black
   :alt: Code style - Black

Summary
-------

**Python** >=3.11

`Project documentation <https://kaiju-auth.readthedocs.io/en/latest/>`_

User authentication, permissions, groups, sessions.

Use `pip install kaiju-auth` to install the package.

For development run the init script `tools/init.sh`.
It will setup git hooks and install the dev version of the project.

See `developer guide <https://kaiju-auth.readthedocs.io/en/latest/#developer-guide>`_ for details.
