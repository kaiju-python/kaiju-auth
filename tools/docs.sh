#!/usr/bin/env sh

# Build API documentation locally using Sphinx
#
# Resulting documentation will be stored at `docs/build/html/`.
#
# Example:
#
#   docs/docs.sh
#

set -e

main() {

  CUR_DIR="${PWD}"

  echo 'Building documentation locally ...'

  cd docs
  pip install -r requirements.txt
  rm -r build/html || true
  rm source/kaiju_auth.* || true
  rm source/modules.rst || true
  make html
  rm source/kaiju_auth.* || true
  rm source/modules.rst || true
  cd ..

  echo 'Documentation index:'
  echo "file://${CUR_DIR}/docs/build/html/index.html"
  echo 'OK'

}

main
