#!/usr/bin/env sh

# Library functions

set -e

# Set git repository attributes such as username and GPG signing
git_set_attributes() {

  if [ -n "${GIT_USERNAME}" ]; then
    git config user.name "${GIT_USERNAME}"
  fi

  if [ -n "${GIT_EMAIL}" ]; then
    git config user.email "${GIT_EMAIL}"
  fi

  # GPG signing should only be set if all the values are filled

  for value in "${GIT_GPG_PROGRAM}" "${GIT_GPG_KEY}"; do
    if [ -z "${value}" ]; then
      return
    fi
  done

  git config init.defaultBranch master
  git config push.followTags true
  git config tag.forceSignAnnotated true
  git config user.gpgsign true
  git config gpg.program "${GIT_GPG_PROGRAM}"
  git config user.signingkey "${GIT_GPG_KEY}"

}

# Make an initial commit for a new project
git_make_initial_commit() {

  COMMIT_MSG='Initial commit'

  git add * || true
  git add .* || true
  git add .gitlab || true
  git add -f .editorconfig .gitignore .pre-commit-config.yaml .pre-commit-config-ci.yaml .bumpversion.cfg || true
  git add -f .dockerignore || true
  git add -f .gitlab-ci.yml || true
  pre-commit run --hook-stage commit || true
  SKIP="requirements-base,requirements-dev" git commit -am "${COMMIT_MSG}"

}

# Update .idea files with project files
idea_update_project_files() {

  mkdir .idea/runConfigurations || true
  cp -n etc/idea/runConfigurations/* .idea/runConfigurations/ || true
  cp etc/idea/kaiju_auth.iml .idea/ || true

}

# Install development version of the current project
setup_dev_project() {

  pip install -U pip setuptools || true
  if [[ -f "requirements/dev.txt" ]]; then
    pip install -r requirements/dev.txt
  fi
  pip install -e .[dev,test]
  pre-commit install --hook-type pre-commit --hook-type commit-msg --hook-type post-commit

}

# Initialize the project for development
init_project() {

  idea_update_project_files
  git init
  git_set_attributes
  setup_dev_project

}

# Build a docker image for the project
build_docker_image() {

  # checking daemon availability first

  docker ps

  REVISION=$(git rev-parse --short HEAD)
  VERSION=$(git --no-pager tag --points-at | head -n 1)
  TIMESTAMP=$(date +"%Y-%m-%d-%H-%M-%S")

  if [ -n "${VERSION}" ]; then
    docker build \
      --build-arg "VERSION=${VERSION}" \
      --build-arg "REVISION=${REVISION}" \
      --build-arg "TIMESTAMP=${TIMESTAMP}" \
      -t "${DOCKER_REGISTRY_ADDR}/kaiju-python/kaiju-auth:latest" \
      -t "${DOCKER_REGISTRY_ADDR}/kaiju-python/kaiju-auth:${REVISION}" \
      -t "${DOCKER_REGISTRY_ADDR}/kaiju-python/kaiju-auth:${VERSION}" \
      .
  else
    docker build \
      --build-arg "VERSION=${VERSION}" \
      --build-arg "REVISION=${REVISION}" \
      --build-arg "TIMESTAMP=${TIMESTAMP}" \
      -t "${DOCKER_REGISTRY_ADDR}/kaiju-python/kaiju-auth:latest" \
      -t "${DOCKER_REGISTRY_ADDR}/kaiju-python/kaiju-auth:${REVISION}" \
      .
  fi

}

# print a changelog between two recent tags
print_changelog() {

  pkg=$(basename "$(git rev-parse --show-toplevel)")
  tag=$(git describe --abbrev=0 --tags)
  prev_tag=$(git describe --abbrev=0 --tags --exclude="${tag}")

  printf '\n*%s v. %s*\n\n' "${pkg}" "${tag}"

  git log --oneline --pretty="format:%s" "${prev_tag}...${tag}" | cat | grep -E '(feat:.+)' | awk '{print "- "$0}'

  printf '\n'

  git log --oneline --pretty="format:%s" "${prev_tag}...${tag}" | cat | grep -E '(fix:.+)' | awk '{print "- "$0}'

  printf '\n###\n'

}
